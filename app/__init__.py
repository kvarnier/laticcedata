from flask import Flask
from datetime import date
from flask_security import Security, SQLAlchemyUserDatastore, \
login_required, roles_accepted
from flask_sqlalchemy import SQLAlchemy
from flask_mail import Mail
from app.forms import ExtendedRegisterForm, ExtendedLoginForm, ExtendedForgotPasswordForm, ExtendedResetPasswordForm


application = Flask(__name__)
db = SQLAlchemy(application)

from app.models import User,Role

application.config.from_pyfile('../config.cfg')
mail = Mail()
mail.init_app(application)
user_datastore = SQLAlchemyUserDatastore(db, User, Role)

from app.views import *

# creation de la base de données si inexistante
@application.before_first_request
def create_db():
    db.create_all()
    user_datastore.find_or_create_role(name="admin", description='full right')
    user_datastore.find_or_create_role(name="member", description='Read, right, export')
    user_datastore.find_or_create_role(name="partner", description='Read, Export')
    db.session.commit()

security = Security(application, user_datastore, login_form = ExtendedLoginForm, confirm_register_form = ExtendedRegisterForm,forgot_password_form = ExtendedForgotPasswordForm, reset_password_form= ExtendedResetPasswordForm)

