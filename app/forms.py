from flask_security.forms import RegisterForm , LoginForm, ConfirmRegisterForm,ForgotPasswordForm, ResetPasswordForm,Required
from wtforms import Form, BooleanField, StringField, PasswordField,SubmitField,RadioField, validators


class ExtendedLoginForm(LoginForm):
    submit = SubmitField('Se connecter')
    remember = BooleanField('Se rappeller de moi')

class ExtendedRegisterForm(ConfirmRegisterForm):
    first_name = StringField('first', [Required()])
    last_name = StringField('last', [Required()])
    company =  StringField('',[Required()])
    function = StringField('',[Required()])
    submit = SubmitField("S'inscrire")
    remember = BooleanField('Se rappeller de moi')

class ExtendedForgotPasswordForm(ForgotPasswordForm):
    submit = SubmitField("Récuperer le mot de passe")


class ExtendedResetPasswordForm(ResetPasswordForm):
    submit = SubmitField("Réinitialiser le mot de passe")
  