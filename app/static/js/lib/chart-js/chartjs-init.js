( function ( $ ) {
	"use strict";

	//Team chart
	var ctx = document.getElementById( "repartion" );
	ctx.height = 150;
	var myChart = new Chart( ctx, {
		type: 'doughnut',
		data: {
			datasets: [ {
				data: [ 23, 25, 20, 12 ],
				backgroundColor: [
                                    "rgba(29, 185, 84, 1)",
                                    "rgba(240, 193, 75, 1)",
                                    "rgba(185, 9, 11, 1)",
                                    "rgba(255,61,2,1)"
                                ],
				hoverBackgroundColor: [
                                    "rgba(29, 185, 84, 1)",
                                    "rgba(240, 193, 75, 1)",
                                    "rgba(185, 9, 11, 1)",
                                    "rgba(255,61,2,1)"
                                ]

                            } ],
			labels: [
                            "Spotify",
                            "Amazon",
							"Netflix", 
							"Google Play",
							"Itunes",
                        ]
		},
		options: {
			responsive: true
		}
	} );


	//Evolution BDD
	var ctx = document.getElementById( "evolutionBDD" );
	ctx.height = 150;
	var myChart = new Chart( ctx, {
		type: 'line',
		data: {
			labels: [ "2016", "2017", "2018", "2019","2020" ],
			type: 'line',
			defaultFontFamily: 'Montserrat',
			datasets: [ {
				label: "Musique",
				data: [ 0, 2302, 8372, 10273, 18178],
				backgroundColor: 'transparent',
				borderColor: 'rgba(38, 218, 210, 1)',
				borderWidth: 3,
				pointStyle: 'circle',
				pointRadius: 5,
				pointBorderColor: 'transparent',
				pointBackgroundColor: 'rgba(38, 218, 210, 1)',
                    }, {
				label: "Livres",
				data: [ 0, 167, 2929, 3227, 5729],
				backgroundColor: 'transparent',
				borderColor: 'rgba(255, 182, 77, 1)',
				borderWidth: 3,
				pointStyle: 'circle',
				pointRadius: 5,
				pointBorderColor: 'transparent',
				pointBackgroundColor: 'rgba(255, 182, 77, 1)',
				},  {
				label: "Audiovisuel",
				data: [ 0, 82, 2278, 7281, 9847],
				backgroundColor: 'transparent',
				borderColor: 'rgba(252, 97, 128, 1)',
				borderWidth: 3,
				pointStyle: 'circle',
				pointRadius: 5,
				pointBorderColor: 'transparent',
				pointBackgroundColor: 'rgba(252, 97, 128, 1)',
				},
				
				
				]
		},
		options: {
			responsive: true,

			tooltips: {
				mode: 'index',
				titleFontSize: 12,
				titleFontColor: '#000',
				bodyFontColor: '#000',
				backgroundColor: '#fff',
				titleFontFamily: 'Montserrat',
				bodyFontFamily: 'Montserrat',
				cornerRadius: 3,
				intersect: false,
			},
			legend: {
				display: true,
				labels: {
					usePointStyle: true,
					fontFamily: 'Montserrat',
				},
			},
			scales: {
				xAxes: [ {
					display: true,
					gridLines: {
						display: false,
						drawBorder: false
					},
					scaleLabel: {
						display: false,
						labelString: 'Month'
					}
                        } ],
				yAxes: [ {
					display: true,
					gridLines: {
						display: false,
						drawBorder: false
					},
					scaleLabel: {
						display: true,
						labelString: 'Nombre de données'
					}
                        } ]
			},
			title: {
				display: false,
				text: 'Normal Legend'
			}
		}
	} );







	//line chart
	var ctx = document.getElementById( "lineChart" );
	ctx.height = 150;
	var myChart = new Chart( ctx, {
		type: 'line',
		data: {
			labels: [ "January", "February", "March", "April", "May", "June", "July" ],
			datasets: [
				{
					label: "My First dataset",
					borderColor: "rgba(0,0,0,.09)",
					borderWidth: "1",
					backgroundColor: "rgba(0,0,0,.07)",
					data: [ 22, 44, 67, 43, 76, 45, 12 ]
                            },
				{
					label: "My Second dataset",
					borderColor: "rgba(0, 123, 255, 0.9)",
					borderWidth: "1",
					backgroundColor: "rgba(0, 123, 255, 0.5)",
					pointHighlightStroke: "rgba(26,179,148,1)",
					data: [ 16, 32, 18, 26, 42, 33, 44 ]
                            }
                        ]
		},
		options: {
			responsive: true,
			tooltips: {
				mode: 'index',
				intersect: false
			},
			hover: {
				mode: 'nearest',
				intersect: true
			}

		}
	} );


	//bar chart
	var ctx = document.getElementById( "barChart" );
	//    ctx.height = 200;
	var myChart = new Chart( ctx, {
		type: 'bar',
		data: {
			labels: [ "January", "February", "March", "April", "May", "June", "July" ],
			datasets: [
				{
					label: "My First dataset",
					data: [ 65, 59, 80, 81, 56, 55, 40 ],
					borderColor: "rgba(0, 123, 255, 0.9)",
					borderWidth: "0",
					backgroundColor: "rgba(0, 123, 255, 0.5)"
                            },
				{
					label: "My Second dataset",
					data: [ 28, 48, 40, 19, 86, 27, 90 ],
					borderColor: "rgba(0,0,0,0.09)",
					borderWidth: "0",
					backgroundColor: "rgba(0,0,0,0.07)"
                            }
                        ]
		},
		options: {
			scales: {
				yAxes: [ {
					ticks: {
						beginAtZero: true
					}
                                } ]
			}
		}
	} );

	//radar chart
	var ctx = document.getElementById( "radarChart" );
	ctx.height = 160;
	var myChart = new Chart( ctx, {
		type: 'radar',
		data: {
			labels: [ [ "Eating", "Dinner" ], [ "Drinking", "Water" ], "Sleeping", [ "Designing", "Graphics" ], "Coding", "Cycling", "Running" ],
			datasets: [
				{
					label: "My First dataset",
					data: [ 65, 59, 66, 45, 56, 55, 40 ],
					borderColor: "rgba(0, 123, 255, 0.6)",
					borderWidth: "1",
					backgroundColor: "rgba(0, 123, 255, 0.4)"
                            },
				{
					label: "My Second dataset",
					data: [ 28, 12, 40, 19, 63, 27, 87 ],
					borderColor: "rgba(0, 123, 255, 0.7",
					borderWidth: "1",
					backgroundColor: "rgba(0, 123, 255, 0.5)"
                            }
                        ]
		},
		options: {
			legend: {
				position: 'top'
			},
			scale: {
				ticks: {
					beginAtZero: true
				}
			}
		}
	} );


	//pie chart
	var ctx = document.getElementById( "pieChart" );
	ctx.height = 300;
	var myChart = new Chart( ctx, {
		type: 'pie',
		data: {
			datasets: [ {
				data: [ 45, 25, 20, 10 ],
				backgroundColor: [
                                    "rgba(255, 182, 77, 1)",
                                    "rgba(0, 123, 255,0.7)",
                                    "rgba(0, 123, 255,0.5)",
                                    "rgba(0,0,0,0.07)"
                                ],
				hoverBackgroundColor: [
                                    "rgba(0, 123, 255,0.9)",
                                    "rgba(0, 123, 255,0.7)",
                                    "rgba(0, 123, 255,0.5)",
                                    "rgba(0,0,0,0.07)"
                                ]

                            } ],
			labels: [
                            "green",
                            "green",
                            "green"
                        ]
		},
		options: {
			responsive: true
		}
	} );

	//doughut chart
	var ctx = document.getElementById( "doughutChart" );
	ctx.height = 150;
	var myChart = new Chart( ctx, {
		type: 'doughnut',
		data: {
			datasets: [ {
				data: [ 45, 25, 20, 10 ],
				backgroundColor: [
                                    "rgba(0, 123, 255,0.9)",
                                    "rgba(0, 123, 255,0.7)",
                                    "rgba(0, 123, 255,0.5)",
                                    "rgba(0,0,0,0.07)"
                                ],
				hoverBackgroundColor: [
                                    "rgba(0, 123, 255,0.9)",
                                    "rgba(0, 123, 255,0.7)",
                                    "rgba(0, 123, 255,0.5)",
                                    "rgba(0,0,0,0.07)"
                                ]

                            } ],
			labels: [
                            "green",
                            "green",
                            "green",
                            "green"
                        ]
		},
		options: {
			responsive: true
		}
	} );

	//polar chart
	var ctx = document.getElementById( "polarChart" );
	ctx.height = 150;
	var myChart = new Chart( ctx, {
		type: 'polarArea',
		data: {
			datasets: [ {
				data: [ 15, 18, 9, 6, 19 ],
				backgroundColor: [
                                    "rgba(0, 123, 255,0.9)",
                                    "rgba(0, 123, 255,0.8)",
                                    "rgba(0, 123, 255,0.7)",
                                    "rgba(0,0,0,0.2)",
                                    "rgba(0, 123, 255,0.5)"
                                ]

                            } ],
			labels: [
                            "green",
                            "green",
                            "green",
                            "green"
                        ]
		},
		options: {
			responsive: true
		}
	} );

	// single bar chart
	var ctx = document.getElementById( "singelBarChart" );
	ctx.height = 150;
	var myChart = new Chart( ctx, {
		type: 'bar',
		data: {
			labels: [ "Sun", "Mon", "Tu", "Wed", "Th", "Fri", "Sat" ],
			datasets: [
				{
					label: "My First dataset",
					data: [ 40, 55, 75, 81, 56, 55, 40 ],
					borderColor: "rgba(0, 123, 255, 0.9)",
					borderWidth: "0",
					backgroundColor: "rgba(0, 123, 255, 0.5)"
                            }
                        ]
		},
		options: {
			scales: {
				yAxes: [ {
					ticks: {
						beginAtZero: true
					}
                                } ]
			}
		}
	} );




} )( jQuery );
