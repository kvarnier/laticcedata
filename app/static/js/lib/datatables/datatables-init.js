$(document).ready(function() {
    var table =  $('#musicTable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'csv', 'excel',
        ],
        

        "displayLength": 25,
        "language": {
            "search": "Rechercher:",
            "infoEmpty": "Pas de données",
            "infoFiltered": " - filtre sur _MAX_ entrées", 
            "info": "Page _PAGE_ sur _PAGES_",
            "paginate": {
                "next": "Suivant",
                "previous" : "Précedent"
              },
            "processing": "Chargement en cours",
            
            "zeroRecords": "Pas de données trouvés"
          },
          "processing": true,
          
        ajax: {
      "url": "api/music",
      "type": "GET",
      "error": function (e) {
      },
      "dataSrc": function (d) {
         return d
      }
      },
    });
  
    $('input[type=checkbox]').on( 'change', function (e) {
        e.preventDefault();
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );

        // Toggle the visibility
        column.visible( ! column.visible() );
    } );
   

    $('#bookTable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'csv', 'excel'
        ],
        "displayLength": 25,
        "language": {
            "search": "Rechercher:",
            "infoEmpty": "Pas de données",
            "infoFiltered": " - filtre sur _MAX_ entrées", 
            "info": "Page _PAGE_ sur _PAGES_",
            "paginate": {
                "next": "Suivant",
                "previous" : "Précedent"
              },
            "processing": "Chargement en cours",
            
            "zeroRecords": "Pas de données trouvés"
          },
          "processing": true,
          
        ajax: {
      "url": "/api",
      "type": "GET",
      "error": function (e) {
      },
      "dataSrc": function (d) {
         return d
      }
      },
    });



    // $('button.toggle-vis').on( 'click', function (e) {
    //     e.preventDefault();
 
    //     // Get the column API object
    //     var column = musicTable.column( $(this).attr('data-column') );
 
    //     // Toggle the visibility
    //     column.visible( ! column.visible() );
    // } );

    // $('input.custom-control-input').change(function(e) {
       
    //     if($(this).is(":checked")) { // or if($("#isAgeSelected").attr('checked') == true){
    //         e.preventDefault();
    // var column = musicTable.column( $(this).attr('data-column') );
 
    //     // Toggle the visibility
    //     column.visible( ! column.visible() );
    // }
    // } );

    $("input:checkbox:not(:checked)").each(function() {
    var column = "table ." + $(this).attr("name");
    $(column).remove();
});

$("input:checkbox").click(function(){
    var column = "table ." + $(this).attr("name");
    $(column).toggle();
});

    $('#userTable').DataTable({
        
        "language": {
            "search": "Rechercher:",
            "lengthMenu": "Voir _MENU_ utilisateurs par page",
            "infoEmpty": "Pas de données",
            "infoFiltered": " - filtre sur _MAX_ entrées", 
            "info": "Page _PAGE_ sur _PAGES_",
            "paginate": {
                "next": "Suivant",
                "previous" : "Précedent"
              },
            
            "zeroRecords": "Pas de données trouvés"
          }
       
    });

    
    $('#importTableIndex').DataTable({
        
        "language": {
            "search": "Rechercher:",
            "lengthMenu": "Voir _MENU_ lignes par page",
            "infoEmpty": "Pas de données",
            "infoFiltered": " - filtre sur _MAX_ entrées", 
            "info": "Page _PAGE_ sur _PAGES_",
            "paginate": {
                "next": "Suivant",
                "previous" : "Précedent"
              },
            
            "zeroRecords": "Pas de données trouvés"
          },
              "order": [[ 0, "desc" ]], 
              "lengthChange": false
       
    });

    $('#importTable').DataTable({
        
        "language": {
            "search": "Rechercher:",
            "lengthMenu": "Voir _MENU_ fichiers  par page",
            "infoEmpty": "Pas de données",
            "infoFiltered": " - filtre sur _MAX_ entrées", 
            "info": "Page _PAGE_ sur _PAGES_",
            "paginate": {
                "next": "Suivant",
                "previous" : "Précedent"
              },
            
            "zeroRecords": "Pas de données trouvés"
          }
       
    });



});

