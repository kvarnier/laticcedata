from app import application, user_datastore, db
from flask import render_template, request, redirect, flash, jsonify
from flask_security import Security,login_required, roles_accepted, current_user
from app.models import User, Music, Book, Movie, Import, MusicIndice
import boto3
import os
from werkzeug.utils import secure_filename
import pandas as pd
from uuid import uuid4
import xlsxwriter
import datetime

def count(media): 
    scanned = media.scan()
    i = 0 
    for a in scanned : 
        i = i + 1 
    return i 

def countByPlatform(media,platform): 
    scanned = media.scan()
    i = 0 
    for item in scanned: 
        if item.platform == platform: 
            i = i+1  
    return i 

def createListByPlatform(media, platform): 
    scanned= media.scan()
    listByPlatform =[]
    for item in scanned :
        if item.platform == platform : 
            listByPlatform.append(item)
    return listByPlatform

def createIndice(media,platform): 
    listByPlatform= createListByPlatform(media,platform)
    sommeIndice = 0

    for item in listByPlatform :
        p = 0 
        v = 0 
        r = 0
        if item.presence is not None :
            p = float(item.presence)

        if item.visibility is not None : 
            v = float(item.visibility)

        if item.recommandation is not None: 
            r = float(item.recommandation)  

        sommeIndice = sommeIndice + p + v + r

    try : 
        indice = sommeIndice / 3 / countByPlatform(media,platform)
    except: 
        indice = 0

    if media == Music :
        if platform == 'Spotify':
            mediaIndice = MusicIndice(str(uuid4()), x_date = datetime.datetime.now(), y_value = str(indice), platform = "Spotify")
        if platform == 'Google Play':
            mediaIndice = MusicIndice(str(uuid4()), x_date = datetime.datetime.now(), y_value = str(indice), platform = "Google Play")
        if platform == 'Apple': 
            mediaIndice = MusicIndice(str(uuid4()), x_date = datetime.datetime.now(), y_value = str(indice), platform = "Apple")
        mediaIndice.save()

        return indice
       

@application.route('/coco')
def coco(): 
    s = createIndice(Music,"Apple")
  
    tot = s
  
    return str(tot)

@application.route('/')
@login_required
def index():
    importItems = Import.scan()
    musicIndiceSpotifyItem = createListByPlatform(MusicIndice, "Spotify")
    musicIndiceGooglePlayItem = createListByPlatform(MusicIndice, "Google Play")
    musicIndiceAppleItem = createListByPlatform(MusicIndice,"Apple")

   
    return render_template("index.html", 
    importItems = importItems,
    musicCount = count(Music),
    movieCount = count(Movie), 
    bookCount = count(Book),
    musicIndiceSpotifyItem= musicIndiceSpotifyItem,
    musicIndiceGooglePlayItem = musicIndiceGooglePlayItem, 
    musicIndiceAppleItem = musicIndiceAppleItem)

@application.route('/music')
@login_required
def music():
    musicItems = Music.scan() 
    return render_template("music.html", musicItems = musicItems)


@application.route('/book')
@login_required
def book():
    return render_template("book.html")

@application.route('/api/music')
@login_required
def musicApi():
    #create an API to load date with Ajax call
    musicItems = Music.scan() 
    data= []
    for item in musicItems: 
        data.append([item.id, 
        item.platform, 
        item.media_type,
        item.artist_name,
        item.artist_url,
        item.artist_isni,
        item.album_name,
        item.barcode,
        item.track_name,
        item.isrc,
        item.item_url,
        item.primary_genre,
        item.rating,
        item.release_date,
        item.reference_list,
        item.pick_up_date[:10],
        item.presence,
        item.visibility,
        item.recommandation,
        item.indice])
    return jsonify(data)

@application.route('/movie')
@login_required
def movie():
    return render_template("movie.html")

def allowed_file(filename):
    ALLOWED_EXTENSIONS = set(['xlsx', 'xls'])
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@application.route('/upload', methods=['GET', 'POST'])
@login_required
@roles_accepted('admin','member')
def upload():
    if request.method == 'POST':
        # check if the post request has the file part
        media = request.form.get('media')
        if media != "Musique": 
            flash("Type de fichier non pris en compte")
        if 'file' not in request.files:
            flash('Pas de fichier')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == '':
            flash('Pas de fichier')
            return redirect(request.url)

        if file and allowed_file(file.filename):
            #save file in tmp folder
            filename = secure_filename(file.filename)
            file.save(os.path.join("app/tmp/", filename))

            #connect to S3 bucket
            s3 = boto3.client('s3')
            bucket = 'elasticbeanstalk-us-east-1-357096547796'
         
            column_names = ['id',
                            'platform',
                            'media_type',
                            'artist_name',
                            'artist_url',
                            'artist_isni',
                            'album_name',
                            'barcode', 
                            'track_name',
                            'isrc',
                            'item_url',
                            'primary_genre',
                            'rating',
                            'release_date',
                            'reference_list',
                            'pick_up_date',
                            'presence',
                            'visibility', 
                            'recommandation',
                            'indice']


            #dataframe read temp file, add columnname and replace empty fields by String Null
            try:
                df = pd.read_excel("app/tmp/{}".format(filename), names = column_names).fillna("")
            except :
                flash("Erreur durant l'importation") 

            #create id list for the new data frame with id generate by uuid
            id = []

            for index, row in df.iterrows(): 
                music_item = Music(str(uuid4()), 
                            platform = str(row["platform"]),
                            media_type= str(row["media_type"]),
                            artist_name = str(row["artist_name"]),
                            artist_url = str(row["artist_url"]),
                            artist_isni = str(row["artist_isni"]),
                            album_name = str(row["album_name"]),
                            barcode = str(row["barcode"]),
                            track_name = str(row["track_name"]),
                            isrc =str(row["isrc"]),
                            item_url = str(row["item_url"]),
                            primary_genre =str(row["primary_genre"]),
                            rating =row["rating"],
                            release_date =str(row["release_date"]),
                            reference_list =str(row["reference_list"]),
                            pick_up_date = str(row["pick_up_date"]),
                            presence =str(row["presence"]),
                            visibility = str(row["visibility"]),
                            recommandation =str(row["recommandation"]),
                            indice =str(row["indice"]))
                
                music_item.save()

                id.append(music_item.id)

            #new date frame with new id                          
            new_df = pd.DataFrame({"id": id})
            df.update(new_df)

            #create excel file and upload to AWS S3
            writer = pd.ExcelWriter("app/tmp/{}".format(filename), engine='xlsxwriter')
            df.to_excel(writer, sheet_name='Sheet1', index=False)
            writer.save()
            

            #create import item
            import_item = Import(str(uuid4()), 
                            file_name = filename,
                            import_date=  datetime.datetime.now(),
                            importer_name =  "{} {}".format(current_user.first_name, current_user.last_name),
                            media_type =  media,
                            input_number = str(df.id.count()),
                         )
            import_item.save()
            itemToS3Name = import_item.id +"_" +filename
            s3.upload_file("app/tmp/{}".format(filename), bucket, 'upload/{}'.format(itemToS3Name))
            #delete tmp file in local
            os.remove("app/tmp/{}".format(filename))
            flash("Importation terminée")
            
            createIndice(Music,"Apple")
            createIndice(Music,"Spotify")
            createIndice(Music,"Google Play") 

            return redirect(request.url)
        
        else :
            flash("Mauvais format de fichier")
    return render_template("upload.html")

@application.route('/uploadManagement', methods=['GET', 'POST'])
@login_required
@roles_accepted('admin', 'member')
def uploadManagement():
    importItems = Import.scan() 
    if request.method == 'POST':
        idImport = request.form.get('idItem')
        #delete import table item
        importToDelete = Import.get(idImport)
        filename= importToDelete.file_name
        importToDelete.delete()

        #connect to S3 bucket
        s3 = boto3.client('s3')
        bucket = 'elasticbeanstalk-us-east-1-357096547796'
        key = "upload/{}".format(idImport + "_" + filename)

        # download file and delete all item in Music Table
        s3.download_file(bucket, key, "app/tmp/{}".format(filename))
        df = pd.read_excel("app/tmp/{}".format(filename))
        for index, row in df.iterrows():
            itemToDelete = Music.get(row[0])
            itemToDelete.delete()
        
        #Delete S3 object and tmp file
        s3.delete_object(Bucket=bucket,Key=key)
        os.remove("app/tmp/{}".format(filename))
        
        #recreate new value for indice
        createIndice(Music,"Apple")
        createIndice(Music,"Spotify")
        createIndice(Music,"Google Play") 
       
    return render_template("uploadManagement.html", importItems = importItems)

@application.route('/userManagement', methods=['GET', 'POST'])
@login_required
@roles_accepted('admin')
def userManagement():
    #user Data for the template
    rows = User.query.all()

    if request.method == 'POST': 
        userRight = request.form.get('userRight')
        userActivation = request.form.get('userActivation')
        userEmail = request.form.get('userMail')

        toassign = user_datastore.find_user(email=userEmail)
        roles ={ "partner" : user_datastore.find_role("partner"), 
        "member" : user_datastore.find_role("member"),
        "admin" : user_datastore.find_role("admin"),          
        }

        #deleting all roles before assignation
        for role in roles : 
            user_datastore.remove_role_from_user(toassign,role)
        
        #roles assignation
        if userRight == "Partenaire" :
            adminroleadmin = user_datastore.find_role("partner")
            user_datastore.add_role_to_user(toassign,adminroleadmin)
        elif userRight == "Chercheur":
            adminroleadmin = user_datastore.find_role("member")
            user_datastore.add_role_to_user(toassign,adminroleadmin)
        elif userRight =="Administrateur":
            adminroleadmin = user_datastore.find_role("admin")
            user_datastore.add_role_to_user(toassign,adminroleadmin)

        #Account activation 
        if userActivation == "Désactiver": 
            user_datastore.deactivate_user(toassign)
        else : 
            user_datastore.activate_user(toassign)
            
        db.session.commit()


    return render_template("userManagement.html", rows= rows)

@application.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
