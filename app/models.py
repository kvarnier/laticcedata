from app import db
from flask_security import Security, SQLAlchemyUserDatastore, \
    UserMixin, RoleMixin, login_required, roles_accepted
from pynamodb.models import Model
from pynamodb.attributes import UnicodeAttribute, UTCDateTimeAttribute



roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

class Role(db.Model, RoleMixin):
    """
    A PostgreSQL Role
    """
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

class User(db.Model, UserMixin):
    """
    A PostgreSQL User
    """
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    company = db.Column(db.String(255))
    function = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

class Music(Model):
    """
    A DynamoDB Music File
    """
    class Meta:
        table_name = "music"
        read_capacity_units = 25
        write_capacity_units = 25

    id = UnicodeAttribute(hash_key=True)
    platform = UnicodeAttribute(null=True)
    media_type = UnicodeAttribute(null= True)
    artist_name = UnicodeAttribute(null= True)
    artist_url = UnicodeAttribute(null= True)
    artist_isni = UnicodeAttribute(null= True)
    album_name = UnicodeAttribute(null= True)
    barcode = UnicodeAttribute(null= True)
    track_name = UnicodeAttribute(null= True)
    isrc = UnicodeAttribute(null= True)
    item_url = UnicodeAttribute(null= True)
    primary_genre = UnicodeAttribute(null= True)
    rating = UnicodeAttribute(null= True)
    release_date = UnicodeAttribute(null= True)
    reference_list = UnicodeAttribute(null= True)
    pick_up_date = UnicodeAttribute(null= True)
    presence = UnicodeAttribute(null= True)
    visibility = UnicodeAttribute(null= True)
    recommandation = UnicodeAttribute(null= True)
    indice = UnicodeAttribute(null= True)

class Book(Model):
    """
    A DynamoDB Book File
    """
    class Meta:
        table_name = "book"
        read_capacity_units = 25
        write_capacity_units = 25

    id = UnicodeAttribute(hash_key=True)
    platform = UnicodeAttribute(null=True)
    media_type = UnicodeAttribute(null= True)
    artist_name = UnicodeAttribute(null= True)
    artist_url = UnicodeAttribute(null= True)
    artist_isni = UnicodeAttribute(null= True)
    album_name = UnicodeAttribute(null= True)
    barcode = UnicodeAttribute(null= True)
    track_name = UnicodeAttribute(null= True)
    isrc = UnicodeAttribute(null= True)
    item_url = UnicodeAttribute(null= True)
    primary_genre = UnicodeAttribute(null= True)
    rating = UnicodeAttribute(null= True)
    release_date = UnicodeAttribute(null= True)
    reference_list = UnicodeAttribute(null= True)
    pick_up_date = UnicodeAttribute(null= True)
    presence = UnicodeAttribute(null= True)
    visibility = UnicodeAttribute(null= True)
    recommandation = UnicodeAttribute(null= True)
    indice = UnicodeAttribute(null= True)

class Movie(Model):
    """
    A DynamoDB Movie File
    """
    class Meta:
        table_name = "movie"
        read_capacity_units = 25
        write_capacity_units = 25

    id = UnicodeAttribute(hash_key=True)
    platform = UnicodeAttribute(null=True)
    media_type = UnicodeAttribute(null= True)
    artist_name = UnicodeAttribute(null= True)
    artist_url = UnicodeAttribute(null= True)
    artist_isni = UnicodeAttribute(null= True)
    album_name = UnicodeAttribute(null= True)
    barcode = UnicodeAttribute(null= True)
    track_name = UnicodeAttribute(null= True)
    isrc = UnicodeAttribute(null= True)
    item_url = UnicodeAttribute(null= True)
    primary_genre = UnicodeAttribute(null= True)
    rating = UnicodeAttribute(null= True)
    release_date = UnicodeAttribute(null= True)
    reference_list = UnicodeAttribute(null= True)
    pick_up_date = UnicodeAttribute(null= True)
    presence = UnicodeAttribute(null= True)
    visibility = UnicodeAttribute(null= True)
    recommandation = UnicodeAttribute(null= True)
    indice = UnicodeAttribute(null= True)


class Import(Model):
    """
    A DynamoDB Import file
    """
    class Meta:
        table_name = "import"
        read_capacity_units = 5
        write_capacity_units = 5

    id = UnicodeAttribute(hash_key=True)
    file_name = UnicodeAttribute(null=True)
    import_date = UTCDateTimeAttribute(null=True)
    importer_name = UnicodeAttribute(null= True)
    media_type = UnicodeAttribute(null= True)
    input_number = UnicodeAttribute(null= True)

class MusicIndice(Model):
    """
    A DynamoDB MusicIndice file
    """
    class Meta:
        table_name = "MusicIndice"
        read_capacity_units = 5
        write_capacity_units = 5
    id = UnicodeAttribute(hash_key=True)
    x_date = UTCDateTimeAttribute(null=True)
    y_value = UnicodeAttribute(null=True)
    platform = UnicodeAttribute(null=True)

if not Music.exists():
    Music.create_table()

if not Book.exists():
    Book.create_table()

if not Movie.exists():
    Movie.create_table()

if not Import.exists():
    Import.create_table()

if not MusicIndice.exists():
    MusicIndice.create_table()



